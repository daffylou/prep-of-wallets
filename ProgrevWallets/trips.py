import random
import lib
import os
from operator import itemgetter

amount_of_sets = int(input('Сколько наборов?: '))
rnd = random.randint
step = int(input('Как долго греть кошелек?(в днях): '))
minimal = int(input('Минимальное кол-во транзакций: '))
maximal = int(input('Максимальное кол-во транзакций: '))
individName = input('Индивидуальные имена для каждого набора? да/нет: ')
login = os.getlogin()
for k in range(amount_of_sets):
    action_list = []
    action = {'mint': lib.action_mint,
              'PancakeSwap': 'https://pancakeswap.finance/swap',
              '1inch': 'https://app.1inch.io/',
              'uniswap': 'https://app.uniswap.org/#/swap',
              'bungee|swap': 'https://bungee.exchange/',
              'traderjoe': 'https://traderjoexyz.com/avalanche/trade',
              'sushiswap': 'https://www.sushi.com/swap',
              'symbiosis|swap': 'https://app.symbiosis.finance/swap',
              'bungee|refuel': 'https://bungee.exchange/refuel',
              'symbiosis|bridge': 'https://app.symbiosis.finance/bridge',
              'omnibtc': 'https://app.omnibtc.finance/swap',
              'woofi': 'https://fi.woo.org/',
              'Polygon zkEVM|official': 'https://wallet.polygon.technology/zkEVM-Bridge/bridge',
              'LayerSwap': 'https://www.layerswap.io/',
              'QuickSwap': 'https://quickswap.exchange/',
              'Orbiter|Polygon zkEVM': 'https://www.orbiter.finance/?source=Polygon&dest=Polygon%20zkEVM',
              'antfarm': 'https://app.antfarm.finance/trade',
              'polygon domen': 'https://app.meroku.org/'}
    result = {}
    for e in action.keys():
        action_list.append(e)


    def mint_value(action, lib, rnd):
        x = action['mint'][rnd(0, len(lib) - 1)]
        return x


    for i in range(0, rnd(minimal, maximal)):
        counter = 0
        counterMint = 0
        num = rnd(0, len(action_list) - 1)
        if len(result.keys()) == 0 and action_list[num] == 'mint':
            num = rnd(1, len(action_list) - 1)

        linkForSource = action[action_list[num]]

        if action_list[num] != 'mint':
            if len(result.keys()) == 0:
                result[linkForSource] = 1
            else:
                for proof in result.keys():
                    if proof[:len(linkForSource)] == linkForSource:
                        counter += 1
                if counter >= 1:
                    result[linkForSource + ' ' * counter] = rnd(2, step)
                else:
                    result[linkForSource] = rnd(2, step)
        elif action_list[num] == 'mint':
            mint_link = mint_value(action, lib.action_mint, rnd)
            for proof in result.keys():
                if proof[:len(mint_link)] == mint_link:
                    counterMint += 1
            if counterMint >= 1:
                result[mint_link + ' ' * counterMint] = rnd(2, step)
            else:
                result[mint_link] = rnd(2, step)

    if action_list.count('mint') <= 2:
        counterMint = 0
        for a in range(rnd(2, 3)):
            mint_link = mint_value(action, lib.action_mint, rnd)
            for proof in result.keys():
                if proof[:len(mint_link)] == mint_link:
                    counterMint += 1
            if counterMint >= 1:
                result[mint_link + ' ' * counterMint] = rnd(2, step)
            else:
                result[mint_link] = rnd(2, step)

    sorted_result = dict(sorted(result.items(), key=itemgetter(1)))

    if individName.lower() == 'да':
        name = input('Название: ')
        with open(rf'C:\Users\{login}\Desktop\{name}' + '.txt', 'w') as f:
            for key, val in sorted_result.items():
                f.write(str(val) + ' ' + key + '\n')
                print(val, key)
        print('__________________'
              '\n|Total action: ', len(sorted_result), '|', sep='')
        with open(rf'C:\Users\{login}\Desktop\{name}' + '.txt', 'a') as f:
            f.write('__________________'
                    '\n|Total action: ' + str(len(sorted_result)) + '|')
    else:
        with open(rf'C:\Users\{login}\Desktop\Set' + str(k + 1) + '.txt', 'w') as f:
            for key, val in sorted_result.items():
                f.write(str(val) + ' ' + key + '\n')
                print(val, key)
        print('__________________'
              '\n|Total action: ', len(sorted_result), '|', sep='')
        with open(rf'C:\Users\{login}\Desktop\Set' + str(k + 1) + '.txt', 'a') as f:
            f.write('__________________'
                    '\n|Total action: ' + str(len(sorted_result)) + '|')
